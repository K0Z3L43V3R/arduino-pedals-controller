/*
  Joystick.cpp

  Copyright (c) 2015, Matthew Heironimus
  Modified by Jan Kaplavka 2016
  - simracing pedals controller
  - 16bit axis values

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Joystick.h"

#if defined(_USING_HID)

#define MINVALUE 0
#define MAXVALUE 1023
#define JOYSTICK_REPORT_ID 0x03
#define JOYSTICK_STATE_SIZE 6

static const uint8_t _hidReportDescriptor[] PROGMEM = {
  
	// Joystick
	0x05, 0x01,			      // USAGE_PAGE (Generic Desktop) 1
	0x09, 0x05,			      // USAGE (GamePad) 4
	0xa1, 0x01,			      // COLLECTION (Application)
	0x85, JOYSTICK_REPORT_ID, //   REPORT_ID (3)

	// X, Y, and Z Axis
	0x15, 0x00,			      //   LOGICAL_MINIMUM (0)
	0x26, 0xff, 0x03,	      //   LOGICAL_MAXIMUM (1023 0xff, 0x03)
	0x75, 0x10,			      //   REPORT_SIZE (16)
	0xA1, 0x00,			      //   COLLECTION (Physical)
	0x09, 0x30,		          //     USAGE (x)
	0x09, 0x31,		          //     USAGE (y)
	0x09, 0x32,		          //     USAGE (z)
	0x95, 0x03,		          //     REPORT_COUNT (3)
	0x81, 0x02,		          //     INPUT (Data,Var,Abs)
	0xc0,				      //   END_COLLECTION
	0xc0				      // END_COLLECTION
};

Joystick_::Joystick_()
{
	// Setup HID report structure
	static HIDSubDescriptor node(_hidReportDescriptor, sizeof(_hidReportDescriptor));
	HID().AppendDescriptor(&node);
	
	// Initalize State
	currentValues.data.clutch = 0;
	currentValues.data.brake = 0;
	currentValues.data.throtle = 0;
	
}

void Joystick_::begin(){
	sendState();
}

void Joystick_::end(){ }

void Joystick_::setClutchAxis(int16_t value){
	// Scale by min(310), max(470)
	currentValues.data.clutch = map(value, 310, 470, MINVALUE, MAXVALUE);
	
	// Calibration - get min and max values used above
	//currentValues.data.clutch = value;
}

void Joystick_::setThrotleAxis(int16_t value){
	// Scale by min max
	currentValues.data.throtle = map(value, 295, 440, MINVALUE, MAXVALUE);
	
	// Calibration - get min and max values used above
	//currentValues.data.throtle = value;
}

void Joystick_::setBrakeAxis(int16_t value){
	// Scale by min max
	currentValues.data.brake = map(value, 250, 415, MINVALUE, MAXVALUE);
	
	// Calibration - get min and max values used above
	//currentValues.data.brake = value;
}

void Joystick_::sendState(){
	// HID().SendReport(Report number, array of values in same order as HID descriptor, length)
	HID().SendReport(JOYSTICK_REPORT_ID, currentValues.buffer, JOYSTICK_STATE_SIZE);	
}

Joystick_ Joystick;

#endif
