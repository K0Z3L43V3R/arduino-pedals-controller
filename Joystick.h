/*
  Joystick.h

  Copyright (c) 2015, Matthew Heironimus
  Modified by Jan Kaplavka 2016
  - simracing pedals controller
  - 16bit axis values

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef JOYSTICK_h
#define JOYSTICK_h

#include "HID.h"

#if ARDUINO < 10606
#error The Joystick library requires Arduino IDE 1.6.6 or greater. Please update your IDE.
#endif

#if !defined(USBCON)
#error The Joystick library can only be used with a USB MCU (e.g. Arduino Leonardo, Arduino Micro, etc.).
#endif

#if !defined(_USING_HID)

#warning "Using legacy HID core (non pluggable)"

#else

class Joystick_
{
private:
	typedef struct AxisData{
		int16_t clutch;
		int16_t brake;
		int16_t throtle;
	};
	
	typedef union CurrentAxisValues{
		AxisData data;
		uint8_t buffer[sizeof(AxisData)];
	};
	
	CurrentAxisValues currentValues;

public:
	Joystick_();

	void begin();
	void end();

	void setThrotleAxis(int16_t value);
	void setClutchAxis(int16_t value);
	void setBrakeAxis(int16_t value);

	void sendState();
};
extern Joystick_ Joystick;

#endif
#endif
