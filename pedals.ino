// Pedals USB controller
//
// version 3.0
// - arduino IDE 1.6.12
// version 2.0
//	- added low pass filter
//
// Jan Kaplavka
// 2016-02-22
//------------------------------------------------------------

#include "Joystick.h"
#include <FilterDerivative.h>
#include <FilterOnePole.h>
#include <Filters.h>
#include <FloatDefine.h>

unsigned long previousMillis = 0;
const long interval = 4;	// 200 HZ

float testFrequency = 6;
FilterOnePole filterThrotleLowpass;
FilterOnePole filterBrakeLowpass;
FilterOnePole filterClutchLowpass;

void setup() {
	Joystick.begin();
  
	pinMode(A0, INPUT_PULLUP);
	pinMode(A1, INPUT_PULLUP);
	pinMode(A2, INPUT_PULLUP);
	pinMode(13, OUTPUT);
  
	filterThrotleLowpass = FilterOnePole( LOWPASS, testFrequency );
	filterBrakeLowpass = FilterOnePole( LOWPASS, testFrequency );
	filterClutchLowpass = FilterOnePole( LOWPASS, testFrequency );
  
	// Turn indicator light on.
	digitalWrite(13, 1);
}

void loop() {
	unsigned long currentMillis = millis();
	
	filterBrakeLowpass.input(analogRead(A2));
	filterThrotleLowpass.input(analogRead(A1));
	filterClutchLowpass.input(analogRead(A0));
	
	if (currentMillis - previousMillis >= interval) {
		Joystick.setClutchAxis(filterClutchLowpass.output()); 
		Joystick.setBrakeAxis(filterBrakeLowpass.output());
		Joystick.setThrotleAxis(filterThrotleLowpass.output());

    /*Joystick.setClutchAxis(analogRead(A0));
    Joystick.setThrotleAxis(analogRead(A1));
    Joystick.setBrakeAxis(analogRead(A2));*/
		
		Joystick.sendState();
		
		previousMillis = currentMillis;
	}
}


