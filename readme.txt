Arduino IDE 1.6.12
Arduino Leonardo (ATmega32U4 16MHz 5V)

Calibrations:
- to get raw values uncoment in Joystic.cpp currentValues.data.clutch = value;
- press pedal to get min / max value 
- read values with DxTweak (example 310 and 470)
- use min / max values in currentValues.data.clutch = map(value, 310, 470, MINVALUE, MAXVALUE);
- repeat for each axis